/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "parse.y" /* yacc.c:339  */
     /* pars1.y    Pascal Parser      Gordon S. Novak Jr.  ; 30 Jul 13   */

/* Copyright (c) 2013 Gordon S. Novak Jr. and
   The University of Texas at Austin. */

/* 14 Feb 01; 01 Oct 04; 02 Mar 07; 27 Feb 08; 24 Jul 09; 02 Aug 12 */

/*
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program; if not, see <http://www.gnu.org/licenses/>.
  */


/* NOTE:   Copy your lexan.l lexical analyzer to this directory.      */

       /* To use:
                     make pars1y              has 1 shift/reduce conflict
                     pars1y                   execute the parser
                     i:=j .
                     ^D                       control-D to end input

                     pars1y                   execute the parser
                     begin i:=j; if i+j then x:=a+b*c else x:=a*b+c; k:=i end.
                     ^D

                     pars1y                   execute the parser
                     if x+y then if y+z then i:=j else k:=2.
                     ^D

           You may copy pars1.y to be parse.y and extend it for your
           assignment.  Then use   make parser   as above.
        */

        /* Yacc reports 1 shift/reduce conflict, due to the ELSE part of
           the IF statement, but Yacc's default resolves it in the right way.*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "token.h"
#include "lexan.h"
#include "symtab.h"
#include "parse.h"

        /* define the type of the Yacc stack element to be TOKEN */

#define YYSTYPE TOKEN
#define YYERROR_VERBOSE 1

TOKEN parseresult;


#line 129 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENTIFIER = 258,
    STRING = 259,
    NUMBER = 260,
    PLUS = 261,
    MINUS = 262,
    TIMES = 263,
    DIVIDE = 264,
    ASSIGN = 265,
    EQ = 266,
    NE = 267,
    LT = 268,
    LE = 269,
    GE = 270,
    GT = 271,
    POINT = 272,
    DOT = 273,
    AND = 274,
    OR = 275,
    NOT = 276,
    DIV = 277,
    MOD = 278,
    IN = 279,
    COMMA = 280,
    SEMICOLON = 281,
    COLON = 282,
    LPAREN = 283,
    RPAREN = 284,
    LBRACKET = 285,
    RBRACKET = 286,
    DOTDOT = 287,
    ARRAY = 288,
    BEGINBEGIN = 289,
    CASE = 290,
    CONST = 291,
    DO = 292,
    DOWNTO = 293,
    ELSE = 294,
    END = 295,
    FILEFILE = 296,
    FOR = 297,
    FUNCTION = 298,
    GOTO = 299,
    IF = 300,
    LABEL = 301,
    NIL = 302,
    OF = 303,
    PACKED = 304,
    PROCEDURE = 305,
    PROGRAM = 306,
    RECORD = 307,
    REPEAT = 308,
    SET = 309,
    THEN = 310,
    TO = 311,
    TYPE = 312,
    UNTIL = 313,
    VAR = 314,
    WHILE = 315,
    WITH = 316
  };
#endif
/* Tokens.  */
#define IDENTIFIER 258
#define STRING 259
#define NUMBER 260
#define PLUS 261
#define MINUS 262
#define TIMES 263
#define DIVIDE 264
#define ASSIGN 265
#define EQ 266
#define NE 267
#define LT 268
#define LE 269
#define GE 270
#define GT 271
#define POINT 272
#define DOT 273
#define AND 274
#define OR 275
#define NOT 276
#define DIV 277
#define MOD 278
#define IN 279
#define COMMA 280
#define SEMICOLON 281
#define COLON 282
#define LPAREN 283
#define RPAREN 284
#define LBRACKET 285
#define RBRACKET 286
#define DOTDOT 287
#define ARRAY 288
#define BEGINBEGIN 289
#define CASE 290
#define CONST 291
#define DO 292
#define DOWNTO 293
#define ELSE 294
#define END 295
#define FILEFILE 296
#define FOR 297
#define FUNCTION 298
#define GOTO 299
#define IF 300
#define LABEL 301
#define NIL 302
#define OF 303
#define PACKED 304
#define PROCEDURE 305
#define PROGRAM 306
#define RECORD 307
#define REPEAT 308
#define SET 309
#define THEN 310
#define TO 311
#define TYPE 312
#define UNTIL 313
#define VAR 314
#define WHILE 315
#define WITH 316

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 299 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   252

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  62
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  50
/* YYNRULES -- Number of rules.  */
#define YYNRULES  109
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  204

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   316

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    83,    83,    87,    88,    93,    94,    97,    98,   101,
     102,   105,   106,   109,   115,   116,   119,   122,   123,   125,
     126,   129,   132,   133,   136,   139,   140,   141,   142,   145,
     146,   149,   150,   153,   154,   155,   158,   159,   160,   163,
     164,   167,   170,   173,   174,   177,   178,   181,   184,   185,
     188,   189,   190,   193,   194,   199,   200,   203,   204,   207,
     208,   209,   210,   211,   212,   213,   216,   219,   220,   223,
     226,   227,   230,   231,   234,   235,   238,   241,   242,   245,
     246,   249,   250,   251,   254,   255,   258,   259,   262,   263,
     264,   265,   266,   267,   268,   271,   272,   275,   276,   279,
     280,   283,   284,   287,   288,   289,   290,   293,   294,   295
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IDENTIFIER", "STRING", "NUMBER", "PLUS",
  "MINUS", "TIMES", "DIVIDE", "ASSIGN", "EQ", "NE", "LT", "LE", "GE", "GT",
  "POINT", "DOT", "AND", "OR", "NOT", "DIV", "MOD", "IN", "COMMA",
  "SEMICOLON", "COLON", "LPAREN", "RPAREN", "LBRACKET", "RBRACKET",
  "DOTDOT", "ARRAY", "BEGINBEGIN", "CASE", "CONST", "DO", "DOWNTO", "ELSE",
  "END", "FILEFILE", "FOR", "FUNCTION", "GOTO", "IF", "LABEL", "NIL", "OF",
  "PACKED", "PROCEDURE", "PROGRAM", "RECORD", "REPEAT", "SET", "THEN",
  "TO", "TYPE", "UNTIL", "VAR", "WHILE", "WITH", "$accept", "program",
  "idlist", "block", "postLabel", "postconst", "postType", "postvar",
  "typeDeclarations", "typeDeclaration", "labelList",
  "constantDeclarations", "constantDeclaration", "varDeclarations",
  "varDeclaration", "type", "typePostPacked", "simpletypeList",
  "simpletype", "fieldList", "typeList", "casePart", "optionalId",
  "optionalCaseList", "caseList", "caseListEntry", "caseConstantList",
  "constant", "constantPostSign", "statements", "statement",
  "stmntPostLabel", "optElse", "funccall", "args", "arg", "direction",
  "assignment", "variable", "indexings", "index", "expressionList",
  "expression", "relation", "simpleExp", "simpleExpPostSign", "sign",
  "term", "factor", "unsignedConstant", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316
};
# endif

#define YYPACT_NINF -163

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-163)))

#define YYTABLE_NINF -74

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -30,    28,    50,    15,  -163,    52,    34,    35,    52,    63,
    -163,    93,    24,    90,    92,   103,    52,    77,  -163,  -163,
    -163,  -163,    57,    85,    24,   110,   111,    97,    24,    97,
      86,   104,  -163,  -163,  -163,   122,   130,    -6,    90,   126,
     127,   143,   -11,   103,   133,   132,    52,  -163,  -163,   158,
     131,    97,  -163,    43,    38,   124,    43,     7,  -163,  -163,
    -163,  -163,  -163,    97,  -163,  -163,  -163,   112,   160,   102,
     115,   161,  -163,  -163,   119,   144,  -163,    24,    97,   163,
    -163,  -163,    92,   106,    29,  -163,  -163,    29,  -163,  -163,
    -163,   162,   153,   164,  -163,   152,   173,  -163,  -163,  -163,
    -163,  -163,    97,   156,    24,  -163,  -163,  -163,  -163,  -163,
    -163,  -163,    97,   115,   102,   115,    97,    24,  -163,  -163,
     174,  -163,  -163,   154,  -163,  -163,   185,    52,   169,   -28,
       5,   175,  -163,  -163,   170,  -163,    51,   177,  -163,   131,
    -163,    97,   167,  -163,   166,  -163,   161,  -163,  -163,  -163,
    -163,  -163,   178,   142,  -163,   203,   181,   171,   183,  -163,
    -163,   190,  -163,  -163,  -163,  -163,  -163,    24,    24,  -163,
    -163,   179,   187,   186,   211,    29,  -163,     5,  -163,  -163,
    -163,   168,   142,  -163,   172,  -163,  -163,    29,  -163,   190,
    -163,  -163,  -163,   189,   191,   192,   190,   193,   190,  -163,
       5,  -163,   194,  -163
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     1,     0,     4,     0,     0,     0,
       3,     0,     0,     0,     0,     0,     0,     0,     6,     8,
      10,    12,    78,     0,     0,     0,     0,     0,     0,     0,
       0,    56,    58,    60,    59,     0,     0,     0,    20,    18,
       0,     0,     0,    15,     0,     0,    23,     2,    83,     0,
       0,     0,    77,    80,     0,     0,    78,     0,    66,   109,
     107,    99,   100,     0,   108,   105,   104,     0,    87,    96,
       0,    98,   102,   103,     0,     0,    13,     0,     0,     0,
       7,    19,     0,     0,     0,     9,    14,     0,    11,    22,
      82,    78,     0,    71,    72,     0,    85,    79,    57,    61,
      75,    74,     0,     0,     0,    88,    89,    91,    93,    92,
      90,    94,     0,     0,    95,     0,     0,     0,    55,    76,
       0,    17,     5,    33,    50,    54,     0,     0,     0,     0,
      40,     0,    28,    25,     0,    52,     0,     0,    69,     0,
      81,     0,     0,   106,    68,    86,    97,   101,    64,    63,
      21,    26,     0,     0,    27,     0,     0,     0,    37,    38,
      16,     0,    53,    51,    24,    70,    84,     0,     0,    62,
      34,     0,    32,     0,     0,     0,    30,    40,    35,    65,
      67,     0,     0,    42,     0,    39,    36,     0,    31,    44,
      29,    41,    43,    46,     0,    49,    44,     0,     0,    45,
      40,    48,     0,    47
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -163,  -163,     1,  -163,   136,   188,   180,   182,   195,  -163,
     146,   196,  -163,   184,  -163,   -76,    95,    44,  -143,  -162,
    -163,  -163,  -163,    33,  -163,  -163,    37,   -75,    96,     2,
     -97,   197,  -163,   -10,    94,  -163,  -163,   206,   -12,   199,
    -163,    98,   -26,  -163,   125,   176,   -65,   123,   128,  -163
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,   156,    17,    18,    19,    20,    21,    42,    43,
      40,    37,    38,    45,    46,   131,   132,   171,   133,   157,
     158,   159,   174,   191,   192,   193,   194,   134,   135,    30,
      31,    32,   169,    65,    92,    93,   102,    34,    66,    52,
      53,    95,    94,   112,    68,    69,    70,    71,    72,    73
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      35,    67,    33,    75,   113,   128,     7,   144,     6,    10,
     172,   137,    35,    35,    33,   186,    35,    44,    33,   136,
     149,     1,   136,    12,   130,    96,    55,    22,    12,    23,
      74,     3,   123,   124,   125,    61,    62,   103,   202,   172,
     155,    22,    35,     5,    33,   100,   126,    44,    16,   113,
       4,    15,   119,    16,   162,     6,   125,   127,    24,     8,
      48,    49,   128,   101,     9,    35,    25,    33,    26,    27,
     179,   180,    24,    51,    48,    49,   142,    28,   129,   118,
      25,   130,    26,    27,    29,    50,   178,    51,   136,    11,
     148,    28,    35,    36,    33,    47,   136,    39,    29,   185,
      22,    59,    60,    61,    62,    35,    41,    33,    61,    62,
     136,   190,    54,    56,   195,    96,    58,   136,    22,    59,
      60,   195,   136,   195,   136,    63,    76,    12,   152,    13,
      77,   136,    78,   136,    91,    59,    60,    61,    62,    14,
      12,    79,    13,    63,    64,   123,   124,   125,    61,    62,
      15,    82,    16,    83,    84,    35,    35,    33,    33,    63,
      87,    90,    64,    15,    99,    16,    12,   104,   120,   115,
     127,   105,   106,   107,   108,   109,   110,   116,    64,    48,
      49,   117,   138,   140,   111,   143,   -53,   -73,   151,   139,
      50,   -73,    51,   162,   124,   125,    61,    62,   141,   153,
     150,   160,   161,   164,   167,   168,   173,   170,   175,   177,
     181,   176,   182,   183,   184,   196,   187,   198,   197,   122,
     189,   200,    85,   203,   154,    80,   188,    88,   121,   199,
      89,    57,   163,   165,    81,   201,   146,   145,    86,   166,
       0,     0,     0,   147,     0,     0,   114,     0,     0,     0,
       0,    98,    97
};

static const yytype_int16 yycheck[] =
{
      12,    27,    12,    29,    69,    33,     5,   104,     3,     8,
     153,    87,    24,    25,    24,   177,    28,    16,    28,    84,
     117,    51,    87,    34,    52,    51,    24,     3,    34,     5,
      28,     3,     3,     4,     5,     6,     7,    63,   200,   182,
      35,     3,    54,    28,    54,    38,    17,    46,    59,   114,
       0,    57,    78,    59,     3,     3,     5,    28,    34,    25,
      17,    18,    33,    56,    29,    77,    42,    77,    44,    45,
     167,   168,    34,    30,    17,    18,   102,    53,    49,    77,
      42,    52,    44,    45,    60,    28,   161,    30,   153,    26,
     116,    53,   104,     3,   104,    18,   161,     5,    60,   175,
       3,     4,     5,     6,     7,   117,     3,   117,     6,     7,
     175,   187,    27,     3,   189,   141,     5,   182,     3,     4,
       5,   196,   187,   198,   189,    28,    40,    34,   127,    36,
      26,   196,    10,   198,     3,     4,     5,     6,     7,    46,
      34,    11,    36,    28,    47,     3,     4,     5,     6,     7,
      57,    25,    59,    26,    11,   167,   168,   167,   168,    28,
      27,     3,    47,    57,    40,    59,    34,    55,     5,     8,
      28,    11,    12,    13,    14,    15,    16,    58,    47,    17,
      18,    37,    29,    31,    24,    29,    32,    25,     3,    25,
      28,    29,    30,     3,     4,     5,     6,     7,    25,    30,
      26,    26,    32,    26,    37,    39,     3,    29,    27,    26,
      31,    40,    25,    27,     3,    26,    48,    25,    27,    83,
      48,    28,    42,    29,   129,    37,   182,    45,    82,   196,
      46,    25,   136,   139,    38,   198,   113,   112,    43,   141,
      -1,    -1,    -1,   115,    -1,    -1,    70,    -1,    -1,    -1,
      -1,    54,    53
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    51,    63,     3,     0,    28,     3,    64,    25,    29,
      64,    26,    34,    36,    46,    57,    59,    65,    66,    67,
      68,    69,     3,     5,    34,    42,    44,    45,    53,    60,
      91,    92,    93,    95,    99,   100,     3,    73,    74,     5,
      72,     3,    70,    71,    64,    75,    76,    18,    17,    18,
      28,    30,   101,   102,    27,    91,     3,    99,     5,     4,
       5,     6,     7,    28,    47,    95,   100,   104,   106,   107,
     108,   109,   110,   111,    91,   104,    40,    26,    10,    11,
      67,    73,    25,    26,    11,    68,    70,    27,    69,    75,
       3,     3,    96,    97,   104,   103,   104,   101,    93,    40,
      38,    56,    98,   104,    55,    11,    12,    13,    14,    15,
      16,    24,   105,   108,   107,     8,    58,    37,    91,   104,
       5,    72,    66,     3,     4,     5,    17,    28,    33,    49,
      52,    77,    78,    80,    89,    90,   108,    77,    29,    25,
      31,    25,   104,    29,    92,   106,   109,   110,   104,    92,
      26,     3,    64,    30,    78,    35,    64,    81,    82,    83,
      26,    32,     3,    90,    26,    96,   103,    37,    39,    94,
      29,    79,    80,     3,    84,    27,    40,    26,    89,    92,
      92,    31,    25,    27,     3,    77,    81,    48,    79,    48,
      77,    85,    86,    87,    88,    89,    26,    27,    25,    85,
      28,    88,    81,    29
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    62,    63,    64,    64,    65,    65,    66,    66,    67,
      67,    68,    68,    69,    70,    70,    71,    72,    72,    73,
      73,    74,    75,    75,    76,    77,    77,    77,    77,    78,
      78,    79,    79,    80,    80,    80,    81,    81,    81,    82,
      82,    83,    84,    85,    85,    86,    86,    87,    88,    88,
      89,    89,    89,    90,    90,    91,    91,    92,    92,    93,
      93,    93,    93,    93,    93,    93,    93,    94,    94,    95,
      96,    96,    97,    97,    98,    98,    99,   100,   100,   101,
     101,   102,   102,   102,   103,   103,   104,   104,   105,   105,
     105,   105,   105,   105,   105,   106,   106,   107,   107,   108,
     108,   109,   109,   110,   110,   110,   110,   111,   111,   111
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     8,     3,     1,     4,     1,     3,     1,     3,
       1,     3,     1,     3,     2,     1,     4,     3,     1,     2,
       1,     4,     2,     1,     4,     1,     2,     2,     1,     6,
       3,     3,     1,     1,     3,     3,     3,     1,     1,     3,
       0,     5,     2,     1,     0,     3,     1,     5,     3,     1,
       1,     2,     1,     1,     1,     3,     1,     3,     1,     1,
       1,     3,     5,     4,     4,     6,     2,     2,     0,     4,
       3,     1,     1,     1,     1,     1,     3,     2,     1,     2,
       1,     3,     2,     1,     3,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     1,     3,     1,     1,
       1,     3,     1,     1,     1,     1,     3,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 84 "parse.y" /* yacc.c:1646  */
    { parseresult = makeProgram(cons((yyvsp[-6]), cons(makeprogn((yyvsp[-5]), (yyvsp[-4])), (yyvsp[-1])))); }
#line 1541 "y.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 87 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1547 "y.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 88 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1553 "y.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 93 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1559 "y.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 97 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1565 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 98 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1571 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 101 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1577 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 105 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1583 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 106 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1589 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 109 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeprogn((yyvsp[-2]), (yyvsp[-1])); }
#line 1595 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 115 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-1]), (yyvsp[0])); }
#line 1601 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 116 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1607 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 119 "parse.y" /* yacc.c:1646  */
    { (yyval) = installType((yyvsp[-3]), (yyvsp[-1])); }
#line 1613 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 122 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1619 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 123 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1625 "y.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 129 "parse.y" /* yacc.c:1646  */
    { loadConstant((yyvsp[-3]), (yyvsp[-1])); }
#line 1631 "y.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 136 "parse.y" /* yacc.c:1646  */
    { declareVars((yyvsp[-3]), (yyvsp[-1])); }
#line 1637 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 139 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1643 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 140 "parse.y" /* yacc.c:1646  */
    { (yyval) = instpoint((yyvsp[-1]), (yyvsp[0])); }
#line 1649 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 141 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1655 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 142 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1661 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 145 "parse.y" /* yacc.c:1646  */
    { (yyval) = instarray((yyvsp[-3]), (yyvsp[0])); }
#line 1667 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 146 "parse.y" /* yacc.c:1646  */
    { (yyval) = instrec((yyvsp[-1])); }
#line 1673 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 149 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1679 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 150 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1685 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 153 "parse.y" /* yacc.c:1646  */
    { (yyval) = findType((yyvsp[0])); }
#line 1691 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 154 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeEnum((yyvsp[-1])); }
#line 1697 "y.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 155 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeSubRange((yyvsp[-2]), (yyvsp[0])); }
#line 1703 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 158 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1709 "y.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 159 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1715 "y.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 163 "parse.y" /* yacc.c:1646  */
    { (yyval) = groupType((yyvsp[0]), (yyvsp[-2])); }
#line 1721 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 199 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1727 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 200 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1733 "y.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 203 "parse.y" /* yacc.c:1646  */
    { (yyval) = insertLabel((yyvsp[-2]), (yyvsp[0])); }
#line 1739 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 207 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1745 "y.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 208 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1751 "y.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 209 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeprogn((yyvsp[-2]), (yyvsp[-1])); }
#line 1757 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 210 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeif((yyvsp[-4]), (yyvsp[-3]), (yyvsp[-1]), (yyvsp[0])); }
#line 1763 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 211 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeWhile((yyvsp[-2]), (yyvsp[0])); }
#line 1769 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 212 "parse.y" /* yacc.c:1646  */
    { (yyval) = makerepeat((yyvsp[-2]), (yyvsp[0])); }
#line 1775 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 215 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeFor((yyvsp[-4]), (yyvsp[-3]), (yyvsp[-2]), (yyvsp[0])); }
#line 1781 "y.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 216 "parse.y" /* yacc.c:1646  */
    { (yyval) = makegoto((yyvsp[0])); }
#line 1787 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 220 "parse.y" /* yacc.c:1646  */
    { (yyval) = NULL; }
#line 1793 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 223 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeFuncCall((yyvsp[-3]), (yyvsp[-1])); }
#line 1799 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 226 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1805 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 227 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1811 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 230 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1817 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 231 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1823 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 234 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1829 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 235 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1835 "y.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 238 "parse.y" /* yacc.c:1646  */
    { (yyval) = binop((yyvsp[-1]), (yyvsp[-2]), (yyvsp[0])); }
#line 1841 "y.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 241 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeAref((yyvsp[-1]), (yyvsp[0])); }
#line 1847 "y.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 245 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-1]), (yyvsp[0])); }
#line 1853 "y.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 246 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1859 "y.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 249 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[-1]); }
#line 1865 "y.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 250 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1871 "y.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 251 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1877 "y.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 254 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[-2]), (yyvsp[0])); }
#line 1883 "y.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 255 "parse.y" /* yacc.c:1646  */
    { (yyval) = cons((yyvsp[0]), NULL); }
#line 1889 "y.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 258 "parse.y" /* yacc.c:1646  */
    { (yyval) = binop((yyvsp[-1]), (yyvsp[-2]), (yyvsp[0])); }
#line 1895 "y.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 259 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1901 "y.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 271 "parse.y" /* yacc.c:1646  */
    { (yyval) = unaryop((yyvsp[-1]), (yyvsp[0])); }
#line 1907 "y.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 272 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1913 "y.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 275 "parse.y" /* yacc.c:1646  */
    { (yyval) = binop((yyvsp[-1]), (yyvsp[-2]), (yyvsp[0])); }
#line 1919 "y.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 276 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1925 "y.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 283 "parse.y" /* yacc.c:1646  */
    { (yyval) = binop((yyvsp[-1]), (yyvsp[-2]), (yyvsp[0])); }
#line 1931 "y.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 289 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[0]); }
#line 1937 "y.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 290 "parse.y" /* yacc.c:1646  */
    { (yyval) = (yyvsp[-1]); }
#line 1943 "y.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 294 "parse.y" /* yacc.c:1646  */
    { (yyval) = makeIntegerToken(0); }
#line 1949 "y.tab.c" /* yacc.c:1646  */
    break;


#line 1953 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 297 "parse.y" /* yacc.c:1906  */


/* You should add your own debugging flags below, and add debugging
   printouts to your programs.

   You will want to change DEBUG to turn off printouts once things
   are working.
  */

#define DEBUGALL 0   /* set bits here for debugging all  */
#define DB_PARSERES 0

#define DB_GROUPTYPE 0
#define DB_INSTALLTYPE 0
#define DB_INSTPOINT 0
#define DB_INSTREC 0
#define DB_INSTARRAY 0
#define DB_MAKEENUM 0
#define DB_MAKESUBRANGE 0
#define DB_MAKEGOTO 0
#define DB_INSTALLANDGETLABEL 0
#define DB_LOADCONSTANT 0
#define DB_INSERTLABEL 0
#define DB_GETCONSTANT 0
#define DB_DECLAREVARS 0
#define DB_MAKEAREF 0
#define DB_FINDTYPE 0
#define DB_TYPIFYID 0
#define DB_MAKEFOR 0
#define DB_MAKEREPEAT 0
#define DB_CONS 0
#define DB_BINOP 0
#define DB_MAKEFLOAT 0
#define DB_MAKEFIX 0
#define DB_UNARYOP 0
#define DB_MAKEWHILE 0
#define DB_MAKEIF 0
#define DB_MAKEPROGN 0
#define DB_MAKEFUNCCALL 0
#define DB_MAKEPROGRAM 0

int labelnumber = 0;  /* sequential counter for internal label numbers */
TOKEN constantsList = NULL;
TOKEN labelList = NULL;

   /*  Note: you should add to the above values and insert debugging
       printouts in your routines similar to those that are shown here.     */

TOKEN makeReservedToken(int which) {
  TOKEN tok = talloc();
  tok->tokentype = RESERVED;
  tok->datatype = INTEGER;
  tok->whichval = which;
  return tok;
}

TOKEN makeOperatorToken(int which) {
  TOKEN tok = talloc();
  tok->tokentype = OPERATOR;
  tok->datatype = INTEGER;
  tok->whichval = which;
  return tok;
}

TOKEN makeIntegerToken(int val) {
  TOKEN tok = talloc();
  tok->tokentype = NUMBERTOK;
  tok->datatype = INTEGER;
  tok->intval = val;
  return tok;
}


TOKEN groupType(TOKEN type, TOKEN idList) {
  if (DEBUGALL | DB_GROUPTYPE) { printf("GROUPING TYPE: %s\n", type->stringval); }
  type->operands = idList;
  return type;
}

TOKEN installType(TOKEN typeId, TOKEN type) {
  if (DEBUGALL | DB_INSTALLTYPE) { printf("INSTALLING TYPE: %s\n", typeId->stringval);}

  SYMBOL typeSym = searchins(typeId->stringval);
  typeSym->datatype = type->symtype;
  typeSym->kind = TYPESYM;
  typeSym->size = type->symtype->size;

  return typeId;
}

TOKEN instpoint(TOKEN tok, TOKEN typename) {
  if (DEBUGALL | DB_INSTPOINT) { printf("INSTALLING POINT to: %s\n", typename->stringval);}
  SYMBOL typeSym = searchins(typename->stringval);
  
  SYMBOL pointSym = symalloc();
  pointSym->kind = POINTERSYM;
  pointSym->datatype = typeSym;
  pointSym->size = 8;
  
  tok = talloc();
  tok->symtype = pointSym;

  return tok;
}

TOKEN instrec(TOKEN fieldList) {
  if (DEBUGALL | DB_INSTREC) { printf("INSTALLING RECORD\n");}

  SYMBOL recordSym = symalloc();
  recordSym->kind = RECORDSYM;

  //int offset = 0;
  int totalSize = 0;
  // go through each field list, get the length
  TOKEN currType = fieldList;
  SYMBOL prev = recordSym;

  while (currType != NULL) {
    SYMBOL type = searchins(currType->stringval);
    if (DEBUGALL | DB_INSTREC) { printf("    CURRENT TYPE: ");}
    dbprsymbol(type);

    int alignSize = alignsize(type);

    int overflow = totalSize % alignSize;
    if (overflow != 0) {
      totalSize += (alignSize - overflow);
    }

    TOKEN currId = currType->operands;

    while (currId != NULL) {
      SYMBOL idSym = makesym(currId->stringval);
      // set its datatype to the currType
      idSym->datatype = type;
      idSym->basicdt = type->basicdt;
      /*if (idSym->datatype->kind == )*/
      // set its offset
      idSym->size = type->size;
      idSym->offset = totalSize;

      if (DEBUGALL | DB_INSTREC) { printf("      CURRENT ID: %s\n", idSym->namestring);}
      if (DEBUGALL | DB_INSTREC) { printf("      size: %d; offset: %d\n", idSym->size, idSym->offset);}

      totalSize += idSym->size;

      prev->link = idSym;
      prev = idSym;
      currId = currId->link;
    }

    currType = currType->link;
  }

  recordSym->datatype = recordSym->link;
  recordSym->link = NULL;

  int overflow = totalSize % 16;
  if (overflow != 0) {
    totalSize += 16 - overflow;
  }

  recordSym->size = totalSize;

  fieldList->symtype = recordSym;

  return fieldList;
}

TOKEN instarray(TOKEN boundsList, TOKEN typetok) {
  if (DEBUGALL | DB_INSTARRAY) { printf("INSTALLING ARRAY, typetok has string val: %s\n", typetok->stringval);}

  SYMBOL bounds = boundsList->symtype;
  if (bounds == NULL) {
    bounds = searchst(boundsList->stringval)->datatype;
  }

  if (DEBUGALL | DB_INSTARRAY) { printf("    has bounds: %d to %d\n", bounds->lowbound, bounds->highbound);}

  if (boundsList->link != NULL) {
    if (DEBUGALL | DB_INSTARRAY) { printf("   GOING DEEPER INTO ARR\n");}
    typetok = instarray(boundsList->link, typetok);
  }

  SYMBOL typeSym = typetok->symtype;
  if (typeSym == NULL) {
    typeSym = searchst(typetok->stringval);
  }
  if (DEBUGALL | DB_INSTARRAY) { dbprsymbol(typeSym);}

  SYMBOL arrSym = symalloc();
  arrSym->kind = ARRAYSYM;
  arrSym->datatype = typeSym;
  arrSym->lowbound = bounds->lowbound;
  arrSym->highbound = bounds->highbound;
  arrSym->size = (arrSym->highbound - arrSym->lowbound + 1) * typeSym->size;

  if (DEBUGALL | DB_INSTARRAY) { printf("   ARR SIZE: %d\n", arrSym->size);}

  typetok = talloc();
  typetok->symtype = arrSym;

  return typetok;
}

TOKEN makeEnum(TOKEN enumIdList) {
  if (DEBUGALL | DB_MAKEENUM) { printf("MAKING ENUM\n");}

  TOKEN currTok = enumIdList;
  int currVal = 0;
  
  while (currTok != NULL) {
    TOKEN id = talloc();
    *id = *currTok;

    TOKEN num = makeIntegerToken(currVal);

    if (DEBUGALL | DB_MAKEENUM) { printf("    str: %s, val: %d\n", id->stringval, num->intval);}

    loadConstant(id, num);

    currVal++;
    currTok = currTok->link;
  }
  currVal--;

  SYMBOL enumSym = symalloc();
  enumSym->kind = SUBRANGE;
  enumSym->lowbound = 0;
  enumSym->highbound = currVal;
  enumSym->size = 4;

  enumIdList->symtype = enumSym;

  return enumIdList;
}

TOKEN makeSubRange(TOKEN low, TOKEN high) {
  if (DEBUGALL | DB_MAKESUBRANGE) { printf("MAKING SUBRANGE from %d to %d\n", low->intval, high->intval);}

  SYMBOL enumSym = symalloc();
  enumSym->kind = SUBRANGE;
  enumSym->lowbound = low->intval;
  enumSym->highbound = high->intval;
  enumSym->size = 4;

  low = talloc();
  low->symtype = enumSym;

  return low;
}

TOKEN makegoto(TOKEN labelNum) {
  if (DEBUGALL | DB_MAKEGOTO) { printf("MAKING GOTO\n");}
  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  TOKEN labelTok = installAndGetLabel(labelNum->intval);

  labelNum->intval = labelTok->operands->intval;
  gotoTok->operands = labelNum;
  labelNum->link = NULL;

  return gotoTok;
}

TOKEN installAndGetLabel(int logicalLabelNum) {
  TOKEN currLabel = labelList;

  while (currLabel != NULL) {

    if (currLabel->tokentype == logicalLabelNum) {
      TOKEN num = makeIntegerToken(currLabel->intval);
      TOKEN labelOp = makeOperatorToken(LABELOP);
      labelOp->operands = num;
      return labelOp;
    }

    currLabel = currLabel->link;
  }

  // if here, label was not found
  TOKEN newLabel = talloc();
  newLabel->tokentype = logicalLabelNum;
  newLabel->intval = labelnumber++;

  labelList = cons(newLabel, labelList);

  TOKEN num = makeIntegerToken(newLabel->intval);
  TOKEN labelOp = makeOperatorToken(LABELOP);

  labelOp->operands = num;
  return labelOp;
}

TOKEN loadConstant(TOKEN id, TOKEN num) {
  id->operands = num;
  constantsList = cons(id, constantsList);
  
  if (DEBUGALL | DB_LOADCONSTANT) { 
    printf("loading constant\n");
    printf("identifier\n");
    dbugprinttok(id);
    printf("number\n");
    dbugprinttok(num);
    printf("\n");
  }
}

TOKEN insertLabel(TOKEN label, TOKEN statement) {
  TOKEN labelTok = installAndGetLabel(label->intval);
  TOKEN progn = makeOperatorToken(PROGNOP);
  progn->operands = labelTok;
  labelTok->link = statement;
  return progn;
}

TOKEN getConstant(char name[]) {
  TOKEN curr = constantsList;

  while (curr != NULL) {
    if (strcmp(name, curr->stringval) == 0) {
      return curr->operands;
    }
    curr = curr->link;
  }
}

/* FROM THE COURSE NOTES */
void declareVars(TOKEN idlist, TOKEN typetok) {
  if (DEBUGALL | DB_DECLAREVARS) { printf("DECLARING VARS ");}
  SYMBOL sym, typesym; int align;

  typesym = typetok->symtype;
  if (typesym == NULL) {
    typesym = searchst(typetok->stringval);
  }

  if (DEBUGALL | DB_DECLAREVARS) { printf(" with type: %s\n", typesym->namestring);}
  align = alignsize(typesym);

  while ( idlist != NULL )   /* for each id */
    {
      if (DEBUGALL | DB_DECLAREVARS) { printf("    DECLARING %s\n", idlist->stringval);}
      sym = insertsym(idlist->stringval);
      sym->kind = VARSYM;
      sym->offset = wordaddress(blockoffs[blocknumber], align);
      sym->size = typesym->size;
      blockoffs[blocknumber] = sym->offset + sym->size;
      sym->datatype = typesym;
      sym->basicdt = typesym->basicdt;
      idlist = idlist->link;
    };
}

TOKEN makeAref(TOKEN id, TOKEN indexList) {
  if (DEBUGALL | DB_MAKEAREF) { printf("AREF into: %s\n", id->stringval);}

  SYMBOL currDatatype = searchst(id->stringval)->datatype;
  if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(currDatatype);}

  TOKEN base = id;

  TOKEN currIndex = indexList;

  while (currIndex != NULL) {
    if (DEBUGALL | DB_MAKEAREF) { printf("    CURRENT INDEX: ");}
    if (DEBUGALL | DB_MAKEAREF) { dbugprinttok(currIndex);}
    if (DEBUGALL | DB_MAKEAREF) { printf("     currDatatype: ");}
    if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(currDatatype);}


    if (currIndex->tokentype == OPERATOR && currIndex->whichval == POINTEROP) {
      if (DEBUGALL | DB_MAKEAREF) { printf("        pointing to type: ");}
      SYMBOL pointType = currDatatype->datatype->datatype;
      if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(pointType);}

      TOKEN point = makeOperatorToken(POINTEROP);
      point->operands = base;
      base = point;
      base->symtype = pointType;
      currDatatype = pointType;
    }
    else if (currDatatype->kind == ARRAYSYM) {
      if (DEBUGALL | DB_MAKEAREF) { printf("        into array with type: ");}
      SYMBOL arrType = currDatatype->datatype;
      int units = arrType->size;

      TOKEN arefTok = makeOperatorToken(AREFOP);
      TOKEN offsetTok = currIndex;

      if (currIndex->tokentype == NUMBERTOK) {
        int byteLoc = (currIndex->intval - currDatatype->lowbound) * units;
        if (DEBUGALL | DB_MAKEAREF) { printf("          integer, byteloc is %d\n", byteLoc);}
        offsetTok = makeIntegerToken(byteLoc);
      }
      if (currIndex->tokentype == IDENTIFIERTOK) {
        if (DEBUGALL | DB_MAKEAREF) { printf("          identifier, will construct index expr\n");}

        TOKEN diff = makeOperatorToken(MINUSOP);
        diff->operands = currIndex;
        currIndex->link = makeIntegerToken(currDatatype->lowbound);

        TOKEN times = makeOperatorToken(TIMESOP);
        times->operands = diff;
        diff->link = makeIntegerToken(units);


        offsetTok = times;
      }

      arefTok->operands = base;
      base->link = offsetTok;
      base = arefTok;
      base->symtype = arrType;
      currDatatype = arrType;
    }
    else if (currIndex->tokentype == IDENTIFIERTOK) {
      if (DEBUGALL | DB_MAKEAREF) { printf("        searching for field: %s\n", currIndex->stringval);}

      SYMBOL currField = currDatatype->datatype->datatype;
      if (currField->kind == RECORDSYM) {
        currField = currField->datatype;
      }
      SYMBOL correctField = NULL;

      while (correctField == NULL && currField != NULL) {
        if (DEBUGALL | DB_MAKEAREF) { printf("          current field is: ");}
        if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(currField);}

        if (strcmp(currIndex->stringval, currField->namestring) == 0) {
          if (DEBUGALL | DB_MAKEAREF) { printf("              found it!\n");}
          correctField = currField;
        }

        currField = currField->link;
      }

      TOKEN arefTok = makeOperatorToken(AREFOP);
      TOKEN offsetTok = makeIntegerToken(correctField->offset);
      arefTok->operands = base;
      base->link = offsetTok;
      base = arefTok;
      base->symtype = correctField;
      currDatatype = correctField;
    }

    currIndex = currIndex->link;
  }

  return base;
}

TOKEN findType(TOKEN type) {
  return type;
}

TOKEN typifyId(TOKEN idTok) {
  if (DEBUGALL | DB_TYPIFYID) { printf("typing :"); dbugprinttok(idTok); }

  TOKEN constantTok = getConstant(idTok->stringval);
  if (constantTok != NULL) {
    idTok = talloc();
    *idTok = *constantTok;
  }
  else {
    idTok->datatype = searchst(idTok->stringval)->basicdt;
  }

  return idTok;
}

TOKEN makeFor(TOKEN assignment, TOKEN direction, TOKEN conditionExpr, TOKEN statement) {
  // label and goto tokens
  // it's okay that only one labelnum token exists
  TOKEN labelnum = makeIntegerToken(labelnumber++);

  TOKEN labelTok = makeOperatorToken(LABELOP);
  labelTok->operands = labelnum;

  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  gotoTok->operands = labelnum;

  /* LOOP CONDITION */

  // get the counter
  TOKEN counter = talloc();
  *counter = *(assignment->operands);

  // get the correct comparator
  int whichCompare = LE - OPERATOR_BIAS;
  if (direction->whichval == (DOWNTO - RESERVED_BIAS)) {
    whichCompare = GE - OPERATOR_BIAS;
  }
  TOKEN comparator = makeOperatorToken(whichCompare);

  // see if the condition expr is a constant
  conditionExpr = typifyId(conditionExpr);

  // now link the counter to the expression, to comparator
  counter->link = conditionExpr;
  comparator->operands = counter;

  /* LOOP EXECUTION */

  // create an increment statement
  // make assign, plus ops
  TOKEN incrementTok = makeOperatorToken(ASSIGNOP);
  TOKEN plusTok = makeOperatorToken(PLUSOP);
  // duplicate the count for left and right side of assign
  TOKEN lhsCount = talloc();
  TOKEN rhsCount = talloc();
  *lhsCount = *counter;
  *rhsCount = *counter;
  // make a 1 token
  TOKEN one = makeIntegerToken(1);
  // form the assignment
  rhsCount->link = one;
  plusTok->operands = rhsCount;
  lhsCount->link = plusTok;
  incrementTok->operands = lhsCount;

  // link the statement, the increment, and goto together
  statement->link = incrementTok;
  incrementTok->link = gotoTok;

  // collect them in a progn
  TOKEN loopBody = makeprogn(talloc(), statement);

  /* IF */

  // create an if token
  TOKEN ifTok = makeOperatorToken(IFOP);
  // link loopBody to comparator, then all to the if
  comparator->link = loopBody;
  ifTok->operands = comparator;

  /* PUTTING IT ALL TOGETHER */

  // link the assignment, label, and if together
  assignment->link = labelTok;
  labelTok->link = ifTok;
  // group all into a progn. we're done!
  TOKEN finalLoop = makeprogn(talloc(), assignment);

  if (DEBUGALL | DB_MAKEFOR) { 
    printf("makeFor\n");
    printf("*** counter tok:     "); dbugprinttok(counter);
    printf("*** comparator:      "); dbugprinttok(comparator);
    printf("*** condition expr:  "); dbugprinttok(conditionExpr);
    dbugprinttok(assignment);
    dbugprinttok(assignment->operands);
    dbugprinttok(direction);
    dbugprinttok(conditionExpr);
    dbugprinttok(statement);
  }
  return finalLoop;
}

TOKEN makerepeat(TOKEN statements, TOKEN expr) {
  // label and goto tokens
  // it's okay that only one labelnum token exists
  TOKEN labelnum = makeIntegerToken(labelnumber++);

  TOKEN labelTok = makeOperatorToken(LABELOP);
  labelTok->operands = labelnum;

  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  gotoTok->operands = labelnum;

  // create an if token
  TOKEN ifTok = makeOperatorToken(IFOP);

  // link the expression, goto to if
  ifTok->operands = expr;
  expr->link = gotoTok;

  // group the body statements into a progn
  TOKEN body = makeprogn(talloc(), statements);

  // now string all together
  labelTok->link = body;
  body->link = ifTok;

  // and finally, collect in a progn
  TOKEN repeatCode = makeprogn(talloc(), labelTok);

  if (DEBUGALL | DB_MAKEREPEAT) { 
    printf("makerepeat\n");
    dbugprinttok(expr);    
    dbugprinttok(body);
  }

  return repeatCode;
}

/* add item to front of list */
TOKEN cons(TOKEN item, TOKEN list) {
  item->link = list;
  if (DEBUGALL | DB_CONS) { 
       printf("cons\n");
       dbugprinttok(item);
       dbugprinttok(list);
  };
  return item;
}

/* reduce binary operator */
TOKEN binop(TOKEN op, TOKEN lhs, TOKEN rhs) {
  if (DEBUGALL | DB_BINOP) { 
    printf("IN BINOP\n");
    printf("  left: "); dbugprinttok(lhs);
    printf("  right: "); dbugprinttok(rhs); 
  }

  if (lhs->tokentype == IDENTIFIERTOK) {
    lhs = typifyId(lhs);
  }
  else if (lhs->tokentype == OPERATOR && lhs->whichval == AREFOP) {
    lhs->datatype = lhs->symtype->basicdt;
  }

  if (rhs->tokentype == IDENTIFIERTOK) {
    rhs = typifyId(rhs);
  }
  else if (rhs->tokentype == OPERATOR && rhs->whichval == AREFOP) {
    rhs->datatype = rhs->symtype->basicdt;
  }

  if (DEBUGALL | DB_BINOP) {
    printf(" left basicdt: %d\n", lhs->datatype);
    printf(" right basicdt: %d\n", rhs->datatype);
  }

  if (lhs->datatype != rhs->datatype) {
    if (op->whichval == ASSIGNOP) {
      if (DEBUGALL | DB_BINOP) {
        printf("   assignment\n");
      }
      if (lhs->datatype == INTEGER)
        rhs = makefix(rhs);
      else
        rhs = makefloat(rhs);
    }
    else {
      if (lhs->datatype == INTEGER)
        lhs = makefloat(lhs);
      else
        rhs = makefloat(rhs);
    }
  }

  op->operands = lhs;          /* link operands to operator       */
  lhs->link = rhs;             /* link second operand to first    */
  rhs->link = NULL;            /* terminate operand list */

  op->datatype = lhs->datatype;

  return op;
}

TOKEN makefloat(TOKEN tok) {
  TOKEN floatOP = makeOperatorToken(FLOATOP);
  floatOP->operands = tok;
  return floatOP;
}

TOKEN makefix(TOKEN tok) {
  TOKEN fixOP = makeOperatorToken(FIXOP);
  fixOP->operands = tok;
  return fixOP;
}

TOKEN unaryop(TOKEN op, TOKEN operand) {
  op->operands = operand;
  operand->link = NULL;
  op->datatype = operand->datatype;
  
  return op;
}

TOKEN makeWhile(TOKEN expr, TOKEN statement) {
  TOKEN labelTok = makeOperatorToken(LABELOP);
  TOKEN labelNum = makeIntegerToken(labelnumber++);
  labelTok->operands = labelNum;

  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  gotoTok->operands = makeIntegerToken(labelNum->intval);

  TOKEN ifTok = makeOperatorToken(IFOP);
  TOKEN whileProgn = makeOperatorToken(PROGNOP);

  whileProgn->operands = labelTok;
  labelTok->link = ifTok;
  ifTok->operands = expr;
  expr->link = statement;
  statement->link = gotoTok;

  return whileProgn;
}

TOKEN makeif(TOKEN tok, TOKEN exp, TOKEN thenpart, TOKEN elsepart)
  {  tok->tokentype = OPERATOR;  /* Make it look like an operator   */
     tok->whichval = IFOP;
     if (elsepart != NULL) elsepart->link = NULL;
     thenpart->link = elsepart;
     exp->link = thenpart;
     tok->operands = exp;
    
    if (DEBUGALL | DB_MAKEIF) { 
    printf("makeif\n");
    dbugprinttok(tok);
    dbugprinttok(exp);
    dbugprinttok(thenpart);
    dbugprinttok(elsepart);
     return tok;
   }
 }

TOKEN makeprogn(TOKEN tok, TOKEN statements)
  {  tok->tokentype = OPERATOR;
     tok->whichval = PROGNOP;
     tok->operands = statements;
     if (DEBUGALL | DB_MAKEPROGN) {
      printf("makeprogn\n");
      dbugprinttok(tok);
      dbugprinttok(statements);
     };
     return tok;
   }

TOKEN makeFuncCall(TOKEN func, TOKEN args) {
  if (DEBUGALL | DB_MAKEFUNCCALL) { printf("FUNCALL\n");}
  SYMBOL funcSym = searchst(func->stringval);
  TOKEN funcCall = makeOperatorToken(FUNCALLOP);

  if (strcmp(funcSym->namestring, "new") == 0) {
    if (DEBUGALL | DB_MAKEFUNCCALL) { printf("  FUNC IS new(), on type: ");}
    SYMBOL newedType = searchst(args->stringval)->datatype->datatype->datatype;
    if (DEBUGALL | DB_MAKEFUNCCALL) { dbprsymbol(newedType);}

    
    TOKEN sizeTok = makeIntegerToken(newedType->size);
    funcCall->operands = func;
    func->link = sizeTok;

    TOKEN assignTok = makeOperatorToken(ASSIGNOP);
    assignTok->operands = args;
    args->link = funcCall;

    return assignTok;
  }
  else if ((strcmp(funcSym->namestring, "write") == 0) || strcmp(funcSym->namestring, "writeln") == 0) {
    if (DEBUGALL | DB_MAKEFUNCCALL) { printf("   FUNC is a write[ln](), arg has basic type: %d\n", args->datatype);}
    SYMBOL argType = args->symtype;

    int basicType = -1;

    if (args->tokentype == IDENTIFIERTOK) {
      if (DEBUGALL | DB_MAKEFUNCCALL) { printf("    this is an identifier\n");}
      argType = searchst(args->stringval);
    }

    if (argType != NULL) {
      if (DEBUGALL | DB_MAKEFUNCCALL) { printf("       more complicated, but has basic type: ");}
      SYMBOL basicTypeSym = argType->datatype;
      if (DEBUGALL | DB_MAKEFUNCCALL) { dbprsymbol(basicTypeSym);}

      basicType = basicTypeSym->basicdt;
    }
    else if (args->tokentype == STRINGTOK) {
      if (DEBUGALL | DB_MAKEFUNCCALL) { printf("      this is a string :)\n");}

      basicType = STRING;
    }

    funcCall->operands = func;
    func->link = args;

    if (strcmp(funcSym->namestring, "write") == 0) {
      if (basicType == INTEGER) {
        strcpy(func->stringval, "writei");
      }
      if (basicType == REAL) {
        strcpy(func->stringval, "writef");
      }
      if (basicType == STRING) {
        strcpy(func->stringval, "write");
      }
    }
    else {
      if (basicType == INTEGER) {
        strcpy(func->stringval, "writelni");
      }
      if (basicType == REAL) {
        strcpy(func->stringval, "writelnf");
      }
      if (basicType == STRING) {
        strcpy(func->stringval, "writeln");
      }
    }

    return funcCall;
  }
  else {
    func->link = args;
    funcCall->operands = func;

    funcCall->symtype = funcSym->datatype;
    return funcCall;
  }
}

TOKEN makeProgram(TOKEN program) {
  // make a program op token
  TOKEN programTok = makeOperatorToken(PROGRAMOP);
  // link it to the program, and return!
  programTok->operands = program;

  return programTok;
}

int wordaddress(int n, int wordsize)
  { return ((n + wordsize - 1) / wordsize) * wordsize; }
 
yyerror(s)
  char * s;
  { 
  fputs(s,stderr); putc('\n',stderr);
  }

main()
  { int res;
    initsyms();
    res = yyparse();
    printst();
    printf("yyparse result = %8d\n", res);
    if (DEBUG & DB_PARSERES) dbugprinttok(parseresult);
    ppexpr(parseresult);           /* Pretty-print the result tree */
  }
